import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import Invoice from '../Pages/Invoice';
import MyClass from '../Pages/MyClass';
import Home from '../Pages/Home';
import Login from '../Pages/Login';
import SkillProject from '../Pages/SkillProject';
import Profile from '../Pages/Profile';
import Feeds from '../Pages/Feeds';
import Register from '../Pages/Register';


const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createNativeStackNavigator();


export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login}  options={{headerShown: false}}/>
                <Stack.Screen name="Home" component={Home}/>
                <Stack.Screen name="My Journey" component={MainApp}/>
                <Stack.Screen name="English Edu" component={MyDrawwer}/>
                <Stack.Screen name="Register" component={Register}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
};


const MainApp =()=>(
        <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = 'home';
              } else if (route.name === 'Kelas Saya') {
                iconName =  'book';
              } else if (route.name === 'Invoice') {
                iconName =  'ios-information-circle-outline';
              } else if (route.name === 'Profil') {
                iconName = 'ios-person-circle-sharp';
              }
  
              // You can return any component that you like here!
              return <Ionicons name={iconName} size={size} color={color} />;
    
            },
            tabBarActiveTintColor: 'tomato',
            tabBarInactiveTintColor: 'gray',
          })}
        >   
           <Tab.Screen name="Home" component={Home} options={{headerShown: false}}/>
           <Tab.Screen name="Kelas Saya" component={MyClass} options={{headerShown: false}}/>
           <Tab.Screen name="Invoice" component={Invoice} options={{headerShown: false}}/>
           <Tab.Screen name="Profil" component={Profile} options={{headerShown: false}}/>
        </Tab.Navigator>
);

const MyDrawwer =()=>(
    <Drawwer.Navigator>
        <Drawwer.Screen name="My Journey" component={MainApp} options={{headerShown:false}}/>
        <Drawwer.Screen name="invoice" component={Invoice} options={{headerShown: false}}/>
    </Drawwer.Navigator>
);

      {/* <MaterialCommunityIcons name={iconName} size={size} color={color} />;
                <Entypo name={iconName} size={size} color={color} />;
                <FontAwesome5 name={iconName} size={size} color={color} />; */}