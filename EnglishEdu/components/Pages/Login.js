import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Button,
} from "react-native";

import { Entypo } from "@expo/vector-icons";

const FormField = ({ title, secureTextEntry }) => {
  return (
    <View style={styles.formContainer}>
      <Text style={styles.formAccount}>{title}</Text>
      <View style={styles.formFieldContainer}>
        <TextInput
          placeholder="******************"
          style={styles.formFieldInput}
          secureTextEntry={secureTextEntry}
        />
        {secureTextEntry && <Entypo name="eye" size={24} color="grey" />}
      </View>
    </View>
  );
};

//   const tombolRespon = ({ title, variant = "filled" }) => {
//     const styleVariant = {
//       filled: {
//         container: { backgroundColor: "blue" },
//         text: { color: "white" },
//       },
//       outlined: {
//         container: {
//           backgroundColor: "white",
//           borderWidth: 1,
//           borderColor: "blue",
//         },
//         text: { color: "blue" },
//       },
//     };
//     return (
//       <TouchableOpacity style={[styles.button, styleVariant[variant].container]}>
//         <Text style={[styles.buttonText, styleVariant[variant].text]}>
//           {title}
//         </Text>
//       </TouchableOpacity>
//     );
//   };

const Login = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.lineContainer}></View>
      <View>
        <Image
          style={styles.logoDrawwer}
          source={require("./images/Logo.png")}
        />
      </View>
      <View style={styles.header}>
        <Text style={styles.headerText}>EnglishEdu</Text>
      </View>

      <View style={styles.title}>
        <Text style={styles.titleText}>Login</Text>
      </View>

      <View style={styles.form}>
        <FormField title="Username/Email" />
        <FormField title="Password" secureTextEntry />
      </View>

      <View style={styles.footer}>
        <Button
          style={styles.button}
          title="Login"
          onPress={() =>
            navigation.navigate("English Edu", {
              screen: "My Journey",
              params: {
                screen: "AboutScreen",
              },
            })
          }
        />
        <Text style={styles.footerText}>Atau</Text>
        <Button 
        style={styles.button} 
        title="Register" variant="outlined" 
        onPress={() => navigation.navigate("Register")}
        />
      </View>
      <View style={styles.lineContainer}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 35,
  },
  header: {
    flex: 1,
    justifyContent: "center",
  },
  headerText: {
    fontSize: 35,
    alignSelf: "center",
    fontWeight: "bold",
  },
  title: {
    flex: 1,
    justifyContent: "center",
  },
  titleText: {
    fontSize: 24,
    fontWeight: "bold",
  },
  form: {
    flex: 3,
  },
  formContainer: {
    marginBottom: 18,
  },
  formAccount: {
    fontSize: 14,
    marginBottom: 4,
    fontWeight: "bold",
  },
  formFieldContainer: {
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 11,
    padding: 6,
    flexDirection: "row",
  },
  formFieldInput: {
    fontSize: 14,
    fontWeight: "bold",
    flex: 1,
  },
  footer: {
    marginVertical: 20,
    alignItems: "stretch",
  },
  button: {
    padding: 10,
    backgroundColor: "#003366",
    alignItems: "center",
    margin: 5,
    borderRadius: 12,
  },
  buttonText: {
    fontWeight: "bold",
    width: "100",
    textAlign: "center",
  },
  logoDrawwer: {
    flex: 1,
    height: 60,
    width: 120,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 40,
  },
  footerText: {
    fontSize: 14,
    justifyContent: "center",
    alignSelf: "center",
    fontWeight: "bold",
  },
  lineContainer: {
    flex: 1,
  },
});

export default Login;
