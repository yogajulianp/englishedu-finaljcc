import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Button,
  } from "react-native";

export default function Home() {
    return (
        <View style={styles.container}>
            <View style={styles.TopText}>
                <Image
                style={styles.logoDrawwer}
                source={require("./images/Logo.png")}
                />
                <Text>Home</Text>
            </View>

            <View style={styles.Cover}>

            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: "#00a3fb",
    },
    TopText: {
        flex: 1,
        paddingTop: 30,
        paddingBottom: 10,
        alignItems: "flex-end",
        height: 60,
        flexDirection: "row",
        justifyContent: "space-between",
      },
      logoDrawwer: {
        height: 60,
        width: 120,
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center",
        paddingTop: 40,
      },
      cover: {
          flex: 1,
      }
})
