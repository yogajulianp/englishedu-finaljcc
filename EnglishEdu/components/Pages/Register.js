import React, {useState, useEffect}from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Button,
} from "react-native";

import { Entypo } from "@expo/vector-icons";

// const FormField = ({ title, secureTextEntry, state, setState, placeholder }) => {
//   return (
//     <View style={styles.formContainer}>
//       <Text style={styles.formAccount}>{title}</Text>
//       <View style={styles.formFieldContainer}>
//         <TextInput
//           placeholder={placeholder}
//           style={styles.formFieldInput}
//           secureTextEntry={secureTextEntry}
//           value={state}
//           onChangeText={(value)=>setState(value)}
//         />
//         {secureTextEntry && <Entypo name="eye" size={24} color="grey" />}
//       </View>
//     </View>
//   );
// };






//   const tombolRespon = ({ title, variant = "filled" }) => {
//     const styleVariant = {
//       filled: {
//         container: { backgroundColor: "blue" },
//         text: { color: "white" },
//       },
//       outlined: {
//         container: {
//           backgroundColor: "white",
//           borderWidth: 1,
//           borderColor: "blue",
//         },
//         text: { color: "blue" },
//       },
//     };
//     return (
//       <TouchableOpacity style={[styles.button, styleVariant[variant].container]}>
//         <Text style={[styles.buttonText, styleVariant[variant].text]}>
//           {title}
//         </Text>
//       </TouchableOpacity>
//     );
//   };

const Register = ({ navigation }) => {

  
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [name, setName] = useState("");


    const Submit=()=>{
      const account = {
        email, password, name
      }
      console.log(account);
  }

  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <View>
          <Text style={styles.titleText}>Register</Text>
        </View>
        <View>
          <Image
            style={styles.logoDrawwer}
            source={require("./images/Logo.png")}
          />
        </View>
      </View>

      <View style={styles.formContainer}>
      <Text style={styles.formAccount}>Nama Lengkap</Text>
      <View style={styles.formFieldContainer}>
        <TextInput
          placeholder="Nama Lengkap"
          style={styles.formFieldInput}
          value = {name}
          onChangeText={(value)=>setName(value)}
        />
      </View>
    </View>

    <View style={styles.formContainer}>
      <Text style={styles.formAccount}>Username/Email</Text>
      <View style={styles.formFieldContainer}>
        <TextInput
          placeholder="Username/Email"
          style={styles.formFieldInput}
          value = {email}
          onChangeText={(value)=>setEmail(value)}
        />
      </View>
    </View>

    <View style={styles.formContainer}>
      <Text style={styles.formAccount}>Password</Text>
      <View style={styles.formFieldContainer}>
        <TextInput
          placeholder="Password"
          style={styles.formFieldInput}
          value = {password}
          onChangeText={(value)=>setPassword(value)}
        />
        <Entypo name="eye" size={24} color="grey" />
      </View>
    </View>

    <View style={styles.formContainer}>
      <Text style={styles.formAccount}>Ulangi Password</Text>
      <View style={styles.formFieldContainer}>
        <TextInput
          placeholder="Ulangi Password"
          style={styles.formFieldInput}
        />
        <Entypo name="eye" size={24} color="grey" />
      </View>
    </View>


      {/* <View style={styles.form}>
        <FormField title="Nama Lengkap" 
        placeholder="Nama Lengkap"
        state="email" setState="setEmail"
        />
        <FormField title="Username/Email" 
         placeholder="Username/Email"
         state="email" setState="setEmail"
         />
        <FormField title="Password" secureTextEntry 
         placeholder="Password"
         state="password" setState="setPasword"
        />
        <Text style={styles.formText}>
          Better with variant number and character{" "}
        </Text>
        <FormField title="Ulangi Password" secureTextEntry 
        placeholder="Ulangi Password"
        />
      </View> */}

      <View style={styles.footer}>
        <Button onPress={Submit} 
        title="Register" style={styles.button}
        />
        <View style={{padding:10}}></View>
        <Button
          style={styles.button}
          title="Buat Akun"
          variant="outlined"
          onPress={() =>navigation.navigate("Login")} 
        />
      </View>
      <View style={styles.lineContainer}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 35,
  },
  header: {
    flex: 1,
    justifyContent: "center",
  },
  headerText: {
    fontSize: 35,
    alignSelf: "center",
    fontWeight: "bold",
  },
  title: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 10,
    alignItems: "flex-end",
    height: 60,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  titleText: {
    fontSize: 24,
    fontWeight: "bold",
  },
  form: {
    flex: 6,
  },
  formContainer: {
    marginBottom: 18,
  },
  formAccount: {
    fontSize: 14,
    marginBottom: 4,
    fontWeight: "bold",
  },
  formFieldContainer: {
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 11,
    padding: 6,
    flexDirection: "row",
  },
  formFieldInput: {
    fontSize: 14,
    fontWeight: "bold",
    flex: 1,
  },
  footer: {
    marginVertical: 20,
    alignItems: "stretch",
  },
  button: {
    flex: 1,
    padding: 10,
    backgroundColor: "#003366",
    alignItems: "center",
    margin: 5,
    borderRadius: 12,
  },
  buttonText: {
    fontWeight: "bold",
    width: "100",
    textAlign: "center",
  },
  logoDrawwer: {
    height: 40,
    width: 100,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    paddingTop: 5,
  },
  footerText: {
    fontSize: 14,
    justifyContent: "center",
    alignSelf: "center",
    fontWeight: "bold",
  },
  lineContainer: {
    flex: 1,
  },
  formText: {
    fontSize: 8,
  },
});

export default Register;
