import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Router from './components/Router';

import firebase from "firebase/app";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDmvYAoxS7fNyxuH-BoqXBEpugorLTt2EE",
  authDomain: "finaljccenglishedu.firebaseapp.com",
  projectId: "finaljccenglishedu",
  storageBucket: "finaljccenglishedu.appspot.com",
  messagingSenderId: "794721381952",
  appId: "1:794721381952:web:4b310aec0cb6e3935f39a5"
};

// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}


export default function App() {
  return (
   <Router/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
